import { createApp } from 'vue';
import App from './App.vue'
import router from "./router";
import store from "./store";


import "bulma/css/bulma.css";
	
const FHv3 = createApp(App)	

FHv3.config.productionTip = false

FHv3
.use(router)
.use(store)
// // now we're ready to mount
FHv3.mount('#app')


// import { createApp } from 'vue'
// import App from './App.vue'
// import router from './router'
// import SomeComponent from '@/components/SomeComponent.vue'
// import SomePlugin from '@/plugins/SomePlugin'

// const myV3App = createApp(App)
// myV3App.component('SomeComponent', SomeComponent)
// myV3App
// .use(SomePlugin)
// .use(router)
// // add more functionality to myV3App

// // now we're ready to mount
// myV3App.mount('#app')
