import { createStore } from "vuex";
// import router from "./router.js";
import EventService from "@/services/eventservice.js";
// {"FIRST":"Brian","LAST":"Fescoe","EEID":"33518","OPTIN":"","DEPARTMENT":"DG-IS","LOGGEDIN":true,"CN":"FescoeBr","WEIGHTEDTOTAL":"","EMAIL":"FescoeBr@einstein.edu"}
// Vue.use(Vuex);

export default createStore({
	state: {
    isloggedIn: false,
    isAllowed: false,
    isAdmin: false,
    userDetails: [],
    // userDetails:[{"FIRST":"Brian","LAST":"Fescoe","EEID":"33518","DEPARTMENT":"DG-IS","LOGGEDIN":true,"CN":"FescoeBr","TITLE":"Web Developer","EMAIL":"FescoeBr@einstein.edu"}],
    watchErrors: "",
    thanks: "",
    professors:[],
    loadingstatus: 'notLoading',
    assets:[],
  },
  getters: {
	professors: state => state.requests,
  mediaItems: state => state.assets
  },
  mutations:{
	showProfess(state, theReqs){
      state.professors = theReqs.requests;
    },
    showAssets(state, items){
      state.assets = items.assets;
    },
  },
  actions:{
	getRequests({commit}, reqData){
      // commit('SET_LOADING_STATUS','loading')
      EventService.getProfessor(JSON.stringify(reqData)).then(response => {
        console.log(response.data);
        let requests = response.data;
        for(let i =0; i < requests.length; i++){
          // commit('SET_LOADING_STATUS','notLoading')
          commit("showProfess",{
              requests: requests
            // thanks: "formComplete",
          });
        } // end for
      }).catch(error => {
        console.log("Error adding Requests" + error.response);
      });
    },
      getAssets({commit}, assetData){
      // commit('SET_LOADING_STATUS','loading')
      // console.log(assetData);
      EventService.getAssetFiles(assetData).then(response => {
         
        let kaka = response.data;
        console.log(kaka);
        for(let i =0; i < kaka.length; i++){
         
          // commit('SET_LOADING_STATUS','notLoading')
          commit("showAssets",{
              assets: kaka
            // thanks: "formComplete",
          });
        } // end for
      }).catch(error => {
        console.log("Error adding Requests" + error.response);
      });
    }
  },
});