import axios from "axios";

//const apiClient = 'http://127.0.0.1:54232/actions.cfc'
//const	apiClient: 'http://wstest.slimmer.einstein.edu/PreformTool/dist/CF_API/actions.cfc',
// Note to self CF does not like using anything but the header I have un commented if i use the others it adds a : at the end of the json string, which cause major erros

const apiClient = axios.create({
    baseURL: "http://localhost:8888/wordpressDev/wp-json/wp/v2/",
   // baseURL: 'http://educationtest/NRCR/cf_api/NRCR_api.cfc',
 
  headers: {
    Accept: "application/json",
    "Content-Type": "application/JSON",
     "Content-type": "application/x-www-form-urlencoded" // remember to comment this out if on CF need to figure out cors for lucee
    // "Content-type": "multipart/form-data"
  }
});

export default {
  logMein(userAuth) {
    console.log(userAuth);
    return apiClient.post("?method=processLogin", userAuth);
  },
  addGrivence(grevenceData) {
    console.log(grevenceData);
    return apiClient.post("?method=insGrivence", grevenceData);
  },
  getProfessor(){
    return apiClient.get("professor");
  },
  getAssetFiles(aID){
    return apiClient.get("media/"+aID);
  },
  addNMcomments(NMdata){
    console.log(NMdata);
     return apiClient.post("?method=addNMcom", NMdata);
  }
};